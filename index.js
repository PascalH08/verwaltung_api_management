const express = require('express');
const config = require('./config.json');

const fs = require('fs');
const app = express();

var routes = {};

app.use(express.json());

const start = () => {
  fs.readdirSync('./routes/')
    .filter(module => {
      return module.slice(module.length - 3) === '.js' && !module.startsWith('index');
    })
    .forEach(module => {
      routes[module.split('.')[0]] = require('./routes/' + module);
    });

  for (let route in routes) {
    if (route.startsWith('get'))
      app.use(routes[`${route}`]).get(route.substring(3));
    else if (route.startsWith('post'))
      console.log('hit post');
    else
      console.log('hit default');
  }
  
  console.log('Loaded all routes successfully');

  app.listen(config.port, () => {
      console.log('app started');
  });
};

start();